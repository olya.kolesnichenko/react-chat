import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';

const drawerWidth = 320;

const styles = theme => ({

  messageInputWrapper: {
    width: `calc(100% - ${drawerWidth}px)`,
    position: 'fixed',
    left: 'auto',
    right: 0,
    bottom: 0,
  },
  messageInput: {
    padding: theme.spacing.unit * 2,
  },

});

class MessageInput extends React.Component {


  render() {

    const { classes } = this.props;

    return (

        <div className={classes.messageInputWrapper}>
          <Paper className={classes.messageInput} elevation={6}>
            <Input fullWidth placeholder="Type something..."/>
          </Paper>
        </div>

    );
  }
}

export default withStyles(styles)(MessageInput);
