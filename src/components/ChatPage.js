import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import Chat from "./Chat";
import ChatHeader from "./ChatHeader";
import Sidebar from "./Sidebar";

import { chats, messages } from '../mock-data.json';

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default
  },
});

const ChatPage = ({classes}) => (

  <div className={classes.root}>
    <ChatHeader />
    <Sidebar chats={chats}/>
    <Chat messages={messages}/>
  </div>
);


export default withStyles(styles)(ChatPage);
