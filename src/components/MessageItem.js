import React from 'react';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Avatar from './Avatar';

const styles = theme => ({

  messageWrapper: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 3}px`,
  },
  messageWrapperFromMe: {
    justifyContent: 'flex-end',
    marginRight: theme.spacing.unit *2
  },
  message: {
    maxWidth: '70%',
    minWidth: '10%',
    padding: theme.spacing.unit,
    marginLeft: theme.spacing.unit *2
  },
  messageFromMe: {
    backgroundColor: '#e6dcff'
  },
});

const MessageItem = ({classes, content, sender}) => {

  const isMessageFromMe = sender === 'me';

  const userAvatar = (
    <Avatar colorFrom={sender}>
      {sender}
    </Avatar>
  );
    return (

            <div className={classnames(classes.messageWrapper, isMessageFromMe ? classes.messageWrapperFromMe : '')}>
              {!isMessageFromMe && userAvatar}
              <Paper className={classnames(classes.message, isMessageFromMe ? classes.messageWrapperFromMe : '')}>
                <Typography variant="caption">
                  {sender}
                </Typography>
                <Typography variant="body1">
                  {content}
                </Typography>
              </Paper>
              {isMessageFromMe && userAvatar}
            </div>
          );
        };

export default withStyles(styles)(MessageItem);
