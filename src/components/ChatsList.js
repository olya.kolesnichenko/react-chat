import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';

import ChatsListItem from "./ChatsListItem";

const styles = theme => ({

  chatList:{
    height: 'calc(100% - 56px)',
    overflowY: 'scroll'
  },

});

const ChatsList = ({ classes, chats }) => (

    <List
      className={classes.chatList}>
      {chats.map((chat, index) => (
        <ChatsListItem key={index} {...chat} />
      ))}
    </List>

);

export default withStyles(styles)(ChatsList);
