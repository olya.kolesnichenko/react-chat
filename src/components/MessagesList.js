import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import MessageItem from "./MessageItem";

const styles = theme => ({

  messagesWrapper: {
    width: '100%',
    height: '100%',
    overflowX: 'scroll',
    paddingTop: theme.spacing.unit * 3,
    paddingBottom: '120px'
  },

});

class MessagesList extends React.Component {

  componentDidMount(){
    this.scrollDownHistory();
  }

  componentDidUpdate(){
    this.scrollDownHistory();
  }

  scrollDownHistory() {
    const messagesWrapper = this.refs.messagesWrapper;

    if (messagesWrapper) {
      messagesWrapper.scrollTop = messagesWrapper.scrollHeight;
    }
  }

  render() {

    const { classes, messages } = this.props;

    return (

      <div className={classes.messagesWrapper} ref="messagesWrapper">
        {messages && messages.map((message, index) => (
          <MessageItem key={index} {...message} />
        ))}
      </div>

    );
  }
}

export default withStyles(styles)(MessagesList);
