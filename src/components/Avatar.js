import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import MUIAvatar from '@material-ui/core/Avatar';

import getColor from "../utils/color-from";
import titleInitials from "../utils/title-initials";

const styles = theme => ({

});

const Avatar = ({colorFrom, children, ...rest}) => (


    <MUIAvatar style={{backgroundColor: getColor(colorFrom)}} {...rest}>
      {titleInitials(children)}
    </MUIAvatar>

);

export default withStyles(styles)(Avatar);
