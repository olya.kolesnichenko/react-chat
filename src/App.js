import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import Chat from "./components/Chat";
import ChatHeader from "./components/ChatHeader";
import Sidebar from "./components/Sidebar";

import { chats, messages } from './mock-data.json';

const drawerWidth = 320;

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    position: 'relative',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default
  },
  appFrame: {
    height: 430,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
    height: '100%'
  },
  drawerHeader: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    ...theme.mixins.toolbar,
  },
  chatList:{
    height: 'calc(100% - 56px)',
    overflowY: 'scroll'
  },
  chatLayout: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '64px',
    overflow: 'hidden'
  },
  newChatButton: {
    position: 'absolute',
    left: 'auto',
    right: theme.spacing.unit * 3,
    bottom: theme.spacing.unit * 3 + 48,
  },
  messagesWrapper: {
    width: '100%',
    height: '100%',
    overflowX: 'scroll',
    paddingTop: theme.spacing.unit * 3,
    paddingBottom: '120px'
  },
  messageInputWrapper: {
    width: `calc(100% - ${drawerWidth}px)`,
    position: 'fixed',
    left: 'auto',
    right: 0,
    bottom: 0,
  },
  messageInput: {
    padding: theme.spacing.unit * 2,
  },
  messageWrapper: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 3}px`,
  },
  messageWrapperFromMe: {
    justifyContent: 'flex-end',
    marginRight: theme.spacing.unit *2
  },
  message: {
    maxWidth: '70%',
    minWidth: '10%',
    padding: theme.spacing.unit,
    marginLeft: theme.spacing.unit *2
  },
  messageFromMe: {
    backgroundColor: '#e6dcff'
  },
});

class App extends React.Component {

  render() {
    const { classes } = this.props;

    return (

        <div className={classes.root}>

          <ChatHeader />

          <Sidebar chats={chats}/>

          <Chat messages={messages}/>

        </div>
    );
  }
}

export default withStyles(styles)(App);
